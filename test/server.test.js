const app = require("../server");
const chai = require("chai");
const chaiHttp = require("chai-http");
const assert = require('assert')
const { expect } = chai;
chai.use(chaiHttp);
describe("Server!", () => {
  it("Test api get", done => {
    chai
      .request(app)
      .get("/")
      .end((err, res) => {
        expect(res).to.have.status(200);
        done();
      });
  });

  it("Test api post", done => {
    chai
      .request(app)
      .post("/create")
      .set("content-type", "application/json")
      .send({name:"hoangdev"})
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property("name").equal("hoangdev");
        done();
      })
  })
})