var express = require('express')
var multer = require('multer');
var upload = multer();
var app = express()
var bodyParser = require('body-parser')
var formidable = require('express-formidable');
// respond with "hello world" when a GET request is made to the homepage
// app.use(formidable());
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
app.use(upload.array()); 
app.use(express.static('public'));


app.get('/', (req, res)  => res.send('success'))

app.post('/create', (req, res)  => {
    res.send(req.body)
})

app.listen(3300, () => console.log('Node.js app listening on port 3300!'))

module.exports = app;